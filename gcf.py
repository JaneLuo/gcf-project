def factor(n):
    fac = []
    for i in range(1,n+1):
        if n%i==0:
            #print(i)
            fac.append(i)
    return fac


def gcf(m, n):
    fac1 = factor(n)
    fac2 = factor(m)

    cf = []
    for i in fac1:
        for j in fac2:
            if i==j:
                #print i;
                cf.append(i)
    gcf = 1
    for i in cf:
        if i>gcf:
            gcf = i
    return gcf

print gcf(100,125)
